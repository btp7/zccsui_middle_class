sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/FilterType"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageToast, MessageBox, JSONModel, Filter, FilterOperator, FilterType) {
        "use strict";

        return Controller.extend("zccs.uimiddleclass.controller.View1", {
            onInit: function () {
                var oMessageManager = sap.ui.getCore().getMessageManager(),
                    oMessageModel = oMessageManager.getMessageModel(),
                    oMessageModelBinding = oMessageModel.bindList("/", undefined, [],
                        new Filter("technical", FilterOperator.EQ, true)),
                    oViewModel = new JSONModel({
                        busy: false,
                        hasUIChanges: false,
                        idEmpty: true,
                        order: 0,
                        expression: ""
                    });
                this.getView().setModel(oViewModel, "appView");
                this.getView().setModel(oMessageModel, "message");

                oMessageModelBinding.attachChange(this.onMessageBindingChange, this);
                this._bTechnicalErrors = false;
            },
            onRefresh: function () {
                var oBinding = this.byId("idTableMiddleClass").getBinding("items");

                if (oBinding.hasPendingChanges()) {
                    MessageBox.error(this._getText("refreshNotPossibleMessage"));
                    return;
                }
                oBinding.refresh();
                MessageToast.show(this._getText("refreshSuccessMessage"));
            },

            _getText: function (sTextId, aArgs) {
                return this.getOwnerComponent().getModel("i18n").getResourceBundle().getText(sTextId, aArgs);

            },

            onSearch: function () {
                var oView = this.getView(),
                    sValue = oView.byId("idSearchField").getValue(),
                    oFilter = new Filter("id", FilterOperator.Contains, sValue);

                oView.byId("idTableMiddleClass").getBinding("items").filter(oFilter, FilterType.Application);
            },
            onMessageBindingChange: function (oEvent) {
                var aContexts = oEvent.getSource().getContexts(),
                    aMessages,
                    bMessageOpen = false;

                if (bMessageOpen || !aContexts.length) {
                    return;
                }

                // Extract and remove the technical messages
                aMessages = aContexts.map(function (oContext) {
                    return oContext.getObject();
                });
                sap.ui.getCore().getMessageManager().removeMessages(aMessages);

                this._setUIChanges(true);
                this._bTechnicalErrors = true;
                MessageBox.error(aMessages[0].message, {
                    id: "serviceErrorMessageBox",
                    onClose: function () {
                        bMessageOpen = false;
                    }
                });

                bMessageOpen = true;
            },
            _setUIChanges: function (bHasUIChanges) {
                if (this._bTechnicalErrors) {
                    // If there is currently a technical error, then force 'true'.
                    bHasUIChanges = true;
                } else if (bHasUIChanges === undefined) {
                    bHasUIChanges = this.getView().getModel().hasPendingChanges();
                }
                var oModel = this.getView().getModel("appView");
                oModel.setProperty("/hasUIChanges", bHasUIChanges);
            },
            onCreate: function () {
                var oList = this.byId("idTableMiddleClass"),
                    oBinding = oList.getBinding("items"),
                    oContext = oBinding.create({
                        "id": null,
                        "big_class_id": null,
                        "description_en": null,
                        "description_zf": null,
                        "formula": null,
                        "percentage": null,
                        "active": null,
                        "grade": null,
                        "ordering": null
                    });

                this._setUIChanges();
                this.getView().getModel("appView").setProperty("/idEmpty", true);

                oList.getItems().some(function (oItem) {
                    if (oItem.getBindingContext() === oContext) {
                        oItem.focus();
                        oItem.setSelected(true);
                        return true;
                    }
                });
            },
            onSave: function () {
                var fnSuccess = function () {
                    this._setBusy(false);
                    MessageToast.show(this._getText("changesSentMessage"));
                    this._setUIChanges(false);
                }.bind(this);

                var fnError = function (oError) {
                    this._setBusy(false);
                    this._setUIChanges(false);
                    MessageBox.error(oError.message);
                }.bind(this);

                this._setBusy(true); // Lock UI until submitBatch is resolved.
                this.getView().getModel().submitBatch("updateGroup").then(fnSuccess, fnError);
                this._bTechnicalErrors = false; // If there were technical errors, a new save resets them.
            },
            _setBusy: function (bIsBusy) {
                var oModel = this.getView().getModel("appView");
                oModel.setProperty("/busy", bIsBusy);
            },
            onResetChanges: function () {
                this.byId("idTableMiddleClass").getBinding("items").resetChanges();
                this._bTechnicalErrors = false;
                this._setUIChanges();
            },
            onInputChange: function (oEvt) {
                if (oEvt.getParameter("escPressed")) {
                    this._setUIChanges();
                } else {
                    this._setUIChanges(true);
                    if (oEvt.getSource().getParent().getBindingContext().getProperty("id")) {
                        this.getView().getModel("appView").setProperty("/idEmpty", false);
                    }
                }
            },
            press: function (oEvent) {
            },
            onDelete: function () {
                var oSelected = this.byId("idTableMiddleClass").getSelectedItem();

                if (oSelected) {
                    oSelected.getBindingContext().delete("$auto").then(function () {
                        MessageToast.show(this._getText("deletionSuccessMessage"));
                    }.bind(this), function (oError) {
                        MessageBox.error(oError.message);
                    });
                }
            },

            onPressformulaset: function (oEvent) {
                this._oContext = oEvent.getSource().getBindingContext();
                var sformula = this._oContext.getObject().formula;
                if (!sformula) {
                    sformula = "";
                } else {
                    var aSmallClasses = {};
                    $.ajax({
                        type: "get",
                        async: false,
                        contentType: "application/json",
                        url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/MasterDataService/SmallClasses"),
                        dataType: "json",
                        cache: false,
                        success: function (data, textStatus, jqXHR) { aSmallClasses = data.value; }
                    });             
                    var aSplitData = sformula.split(/[']/g);
                    for (var i = 0; i < aSplitData.length; i++) {
                        var sSmallClasses = aSmallClasses.filter(function (item) {
                            return item.id === aSplitData[i];
                        });
                        if (sSmallClasses.length !== 0) {
                            var sText = sSmallClasses[0].id + "-" + sSmallClasses[0].description_zf;
                            sformula = sformula.replaceAll(aSplitData[i], sText);
                        }
                    }

                }
                this.getView().getModel("appView").setProperty("/expression", sformula);
                this.byId("idDialog").open();
            },

            onCloseInput: function () {
                this.byId("idDialog").close();
            },
            onClearInput: function () {
                this.getView().getModel("appView").setProperty("/expression", "");
            },
            editInput(ch) {
                var sExpression = this.getView().getModel("appView").getProperty("/expression");
                if ("+/*-".indexOf(ch) == -1) {
                    sExpression += ch;
                } else {
                    /*  sExpression += (" " + ch + " "); */
                    sExpression += ch;
                }
                this.getView().getModel("appView").setProperty("/expression", sExpression);
            },
            onListItemPress: function (oEvent) {
                var sText = oEvent.getSource().getTitle();
                sText = "'" + sText + "'";
                var sExpression = this.getView().getModel("appView").getProperty("/expression");
                /* sExpression += (" " + sText + " "); */
                sExpression += sText;
                this.getView().getModel("appView").setProperty("/expression", sExpression);
            },
            onSaveInput: function () {
                var sExpression = this.getView().getModel("appView").getProperty("/expression");
                var aSplitData = sExpression.split(/[']/g);
                aSplitData = aSplitData.filter(function (item, pos) {
                    return aSplitData.indexOf(item) == pos;
                });

                var aSmallClasses = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/MasterDataService/SmallClasses"),
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aSmallClasses = data.value; }
                });
                for (var i = 0; i < aSplitData.length; i++) {
                    var sData = aSplitData[i].slice(aSplitData[i].indexOf("-") + 1);
                    /*    aSplitData[i].some(ai => array.includes(ai)); */
                    var sSmallClasses = aSmallClasses.filter(function (item) {
                        /*   return item.description_zf == aSplitData[i]; */
                        return item.description_zf == sData;
                    });
                    if (sSmallClasses.length !== 0) {
                        /*    var sText = "-" + aSplitData[i]; */
                        var sText = "-" + sData;
                        sExpression = sExpression.replaceAll(sText, "");
                    }
                }
                this._oContext.setProperty("formula", sExpression);
                this.byId("idDialog").close();
                this._setUIChanges(true);
                this.getView().getModel("appView").setProperty("/idEmpty", false);
            },

            onPressdetail: function (oEvent) {
                var sId = oEvent.getSource().getBindingContext().getObject().id;
                if (this.onIdExists(sId)) {
                    var xnavservice = sap.ushell && sap.ushell.Container && sap.ushell.Container.getService("CrossApplicationNavigation");
                    var href = (xnavservice && xnavservice.hrefForExternal({
                        target: { semanticObject: "middle_class_sorting", action: "display" },
                        params: { "objectid": sId }
                    })) || "";

                    var finalUrl = window.location.href.split("#")[0] + href;
                    sap.m.URLHelper.redirect(finalUrl, true);
                } else {
                    MessageBox.error(this._getText("savelinefirstMessage"));
                    /*   Please save this line data first */
                }
            },

            onPressscore: function (oEvent) {
                var sId = oEvent.getSource().getBindingContext().getObject().id;
                if (this.onIdExists(sId)) {


                    var xnavservice = sap.ushell && sap.ushell.Container && sap.ushell.Container.getService("CrossApplicationNavigation");
                    var href = (xnavservice && xnavservice.hrefForExternal({
                        target: { semanticObject: "middle_class_score", action: "Display" },
                        params: { "objectid": sId }
                    })) || "";

                    var finalUrl = window.location.href.split("#")[0] + href;
                    sap.m.URLHelper.redirect(finalUrl, true);
                } else {
                    MessageBox.error(this._getText("savelinefirstMessage"));
                }
            },

            onIdExists: function (sId) {
                var sfilter = "?$filter=(id eq '" + sId + "')";
                var aData = {};
                $.ajax({
                    type: "get",
                    async: false,
                    contentType: "application/json",
                    url: this.getOwnerComponent().getManifestObject().resolveUri("odata/v4/MasterDataService/MiddleClasses") + sfilter,
                    dataType: "json",
                    cache: false,
                    success: function (data, textStatus, jqXHR) { aData = data.value; }
                });
                if (aData.length > 0) {
                    return true;
                } else {
                    return false;
                }

            }
        });
    });
