sap.ui.define(
    [
        "sap/ui/core/mvc/Controller"
    ],
    function(BaseController) {
      "use strict";
  
      return BaseController.extend("zccs.uimiddleclass.controller.controller.App", {
        onInit() {
        }
      });
    }
  );
  