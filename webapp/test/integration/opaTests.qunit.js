/* global QUnit */

sap.ui.require(["zccs/uimiddleclass/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
